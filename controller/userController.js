const userModel = require("../models/userModel")


module.exports.createUser = async function (req, res) {
    try {
        let data = req.body
        let Options = data

        if (Object.keys(data).length === 0) {
            return res.status(400).send({ Status: false, message: "Please provide all the details" })
        }
            Options=data.Options
            if(Options){
                if(!( ["Dinner", "Meeting", "Party", "Birthday celebration"].includes(Options))) {
                  return res.status(400).send({ Status: false, message: "Options must be 'Dinner', 'Meeting','Party' or 'Birthday celebration' only " })
                }
            }
        let savedData = await userModel.create(data)
        return res.status(201).send({ status : true, msg: savedData })   
  }
  catch (error) {
    res.status(500).send({ status: false, error: error.message })
  }
}




module.exports.getUser = async function (req, res) {
    try {
       
        let user = await userModel.find()
        return res.status(200).send({ status: true, data: user }) 
    } catch (err) {
        return res.status(500).send({ status: false, message: err.message })
    }
}



