require('dotenv').config()
const express = require('express');
require('./db/mongoDb')
const app = express();
const router = require('./routes/route')
app.use(express.json());


app.use('/', router)


app.listen(process.env.PORT || 3000, () => {
    console.log(`Express app listening on port ${process.env.PORT || 3000}`);
})  