const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    Name: {
        type: String,
        required: true,
    },
    Profession: {
        type: String,
        required: true,
    },
    Budget: {
        type: Number,
        required: true,
    },
    likes: {
        type: Number,
        required: true,
    },
    Options: {
        type: String,
        required: [true ,"Please provide valid Options"],
        enum: ["Dinner", "Meeting", "Party", "Birthday celebration"]
    },
},);

module.exports = mongoose.model('user', userSchema) 